const db = require('./conexao');

const criarTabelaCidade = async function criarTabelaCidade(){
    await db.connect();

    await db.query('CREATE TABLE cidades (id serial PRIMARY KEY,  nome VARCHAR(50) UNIQUE NOT NULL)');

    await db.end();
    console.log('Tabela criada com sucesso');
}

const inserirCidade = async function inserirCidade(){
    await db.connect();

    const queryInsert = 'INSERT INTO cidades (nome) VALUES ($1)';

    await db.query(queryInsert, ['cidade1']);
    await db.query(queryInsert, ['cidade2']);
    await db.query(queryInsert, ['cidade3']);
    await db.query(queryInsert, ['cidade4']);
    await db.query(queryInsert, ['cidade5']);
    await db.query(queryInsert, ['cidade6']);
    await db.query(queryInsert, ['cidade7']);
    await db.query(queryInsert, ['cidade8']);
    await db.query(queryInsert, ['cidade9']);
    await db.query(queryInsert, ['cidade10']);

    await db.end();
    console.log('cidades inseridas com sucesso');

}

const atualizaCidade = async function atualizaCidade(){
    await db.connect();

    await db.query(`UPDATE cidades SET nome = 'cidade101' WHERE id = 6`);
    await db.end();
    console.log('cidade  atualizada com sucesso');
}

const apagaCidade = async function apagaCidade(){
    await db.connect();
    await db.query('DELETE FROM cidades WHERE id = 8');
    await db.end();
    console.log('Removido com sucesso');
}

const consulta1 = async function consulta1(){
    await db.connect();

    let result;
    result = await db.query('SELECT * FROM cidades');
    await db.end();
    // console.log(result.rows);
    return result.rows;
}

const consulta2 = async function consulta2(){
    await db.connect();
    let result;
    result  = await db.query('SELECT e.id as Empresa_Id, e.nome as Empresa, c.id as Cidade_ID, c.nome as Cidade FROM empresas e, cidades c, empresa_cidade ec WHERE ec.empresa_id = e.id AND ec.cidade_id = c.id');
    await db.end();
    return  result.rows;
}

// criarTabelaCidade();
// inserirCidade();
// atualizaCidade();
// apagaCidade();

module.exports =  [
    criarTabelaCidade,
    inserirCidade,
    atualizaCidade,
    apagaCidade,
    consulta1,
    consulta2
];