const db = require('./conexao');

const criarTabelaEmpresa = async function criarTabelaEmpresa(){
    await db.connect();

    await db.query('CREATE TABLE empresas (id serial PRIMARY KEY,  nome VARCHAR(50) UNIQUE NOT NULL)');

    await db.end();
    console.log('Tabela criada com sucesso');
}

const inserirEmpresa =  async function inserirEmpresa(){
    await db.connect();

    const queryInsert = 'INSERT INTO empresas (nome) VALUES ($1)';

    await db.query(queryInsert, ['empresa1']);
    await db.query(queryInsert, ['empresa2']);
    await db.query(queryInsert, ['empresa3']);
    await db.query(queryInsert, ['empresa4']);
    await db.query(queryInsert, ['empresa5']);
    await db.query(queryInsert, ['empresa6']);
    await db.query(queryInsert, ['empresa7']);
    await db.query(queryInsert, ['empresa8']);
    await db.query(queryInsert, ['empresa9']);
    await db.query(queryInsert, ['empresa10']);

    await db.end();
    console.log('Empresas inseridas com sucesso');

}

const atualizaEmpresa = async function atualizaEmpresa(){
    await db.connect();

    await db.query(`UPDATE empresas SET nome = 'Empresa101' WHERE id = 6`);
    await db.end();
    console.log('Empresa  atualizada com sucesso');
}

const apagaEmpresa = async function apagaEmpresa(){
    await db.connect();
    await db.query('DELETE FROM empresas WHERE  id  = 8');
    await db.end();
    console.log('Removido com sucesso');
}

const consulta1 = async function consulta1(){
    await db.connect();

    let result;
    result = await db.query('SELECT * FROM empresas');
    await db.end();
    // console.log(result.rows);
    return result.rows;
}

const consulta2 = async function consulta2(){
    await db.connect();

    let result;
    result  = await db.query('SELECT e.nome, c.nome FROM empresas e, cidades c, empresa_cidade ec WHERE ec.empresa_id = e.id AND ec.cidade_id = c.id');
    await db.end();
    console.log(result.rows);
}

// criarTabelaEmpresa();
// inserirEmpresa();
atualizaEmpresa();
// apagaEmpresa();
// consulta1();
// consulta2();

module.exports =  [
    criarTabelaEmpresa,
    inserirEmpresa,
    atualizaEmpresa,
    apagaEmpresa,
    consulta1,
    consulta2
];