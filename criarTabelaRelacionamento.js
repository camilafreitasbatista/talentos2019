const db = require('./conexao');

async function createTable(){
    await db.connect();

    await db.query('CREATE TABLE empresa_cidade(empresa_id INTEGER NOT NULL, cidade_id INTEGER NOT NULL, PRIMARY KEY (empresa_id, cidade_id), FOREIGN KEY (empresa_id) REFERENCES empresas(id), FOREIGN KEY (cidade_id) REFERENCES cidades(id))');

    await db.end();
    console.log('Tabela criada com sucesso');
}

async function insert(){
    await db.connect();

    const query = 'INSERT INTO empresa_cidade(empresa_id, cidade_id) VALUES ($1, $2)';

    await db.query(query, [1, 1]);
    await db.query(query, [2, 2]);
    await db.query(query, [3, 3]);
    await db.query(query, [4, 4]);
    await db.query(query, [5, 1]);
    await db.query(query, [9, 6]);
    await db.query(query, [2, 10]);
    await db.query(query, [3, 2]);

    await db.end();
    console.log('Linhas inseridas com sucesso');
}

// createTable();
// insert();

module.exports = [
    createTable,
    insert
];